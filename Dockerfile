FROM fpco/stack-build:latest

RUN stack build base --install-ghc --resolver=nightly-2016-12-31
